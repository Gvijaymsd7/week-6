package com.greatlearning.moviescomingdao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.greatlearning.bean.Movies;
import com.greatlearning.interfaces.AllMovies;
import com.greatlearning.resource.DbResource;

public class MoviesComing implements AllMovies
{
	public List<Movies> getMovies() throws SQLException 
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("select *from moviescoming");
		ResultSet rs = ps.executeQuery();
		List<Movies> ls = new ArrayList<>();

		while (rs.next()) {
			
			Movies emp = new Movies();
			emp.setId(rs.getInt("id"));
			emp.setTitle(rs.getString("title"));
			emp.setYear(rs.getInt("year"));
			emp.setContentRating(rs.getInt("contentRating"));
			emp.setDuration(rs.getString("duration"));
			emp.setReleaseDate(rs.getString("releaseDate"));
			emp.setAverageRating(rs.getInt("averageRating"));
			emp.setOriginalTitle(rs.getString("originalTitle"));
			emp.setImdbRating(rs.getInt("imdbRating"));
			ls.add(emp);
		}
		return ls;
	}catch(Exception e) {
		System.out.println(e);
		return null;
	}
	}

	public int insert(Movies movie) throws SQLException
	{
		try {
			
			Connection con = DbResource.getDbConnection();
		
		PreparedStatement ps = con.prepareStatement("insert into moviescoming values(?,?,?,?,?,?,?,?,?)");

		ps.setInt(1, movie.getId());
		ps.setString(2, movie.getTitle());
		ps.setInt(3, movie.getYear());
		ps.setInt(4, movie.getContentRating());
		ps.setString(5,movie.getDuration());
		ps.setString(6, movie.getReleaseDate());
		ps.setInt(7,movie.getAverageRating());
		ps.setString(8, movie.getOriginalTitle());
		ps.setInt(9,movie.getImdbRating());
		return ps.executeUpdate();
		}catch(Exception e){
			System.out.println(e);
		
		}
		return 0;
	}
	 
	public int update(Movies movie) throws SQLException
	{
		try {
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("update moviescomming set id=? where name=?");
		
		ps.setInt(1, movie.getId());
		ps.setString(2, movie.getTitle());
		return ps.executeUpdate();
		}catch(Exception e)
		{
		System.out.println(e);
			return 0;	
		}
		
	}
	
	public int delete(Movies movie) throws SQLException
	
	{
		try {
			
			Connection con = DbResource.getDbConnection();
		PreparedStatement ps = con.prepareStatement("delete from moviescoming where name=?");
		ps.setString(1, movie.getTitle());
		return ps.executeUpdate();
		}catch (Exception e){
			System.out.println(e);
			return 0;
		}
		
		
	
		}
}

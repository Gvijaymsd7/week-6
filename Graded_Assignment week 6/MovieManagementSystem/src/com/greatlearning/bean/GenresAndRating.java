package com.greatlearning.bean;

public class GenresAndRating {
	int id;
	String genres;
	int rating;
	int gid;
	public GenresAndRating() {
		super();
		// TODO Auto-generated constructor stub
	}
	public GenresAndRating(int id, String genres, int rating, int gid) {
		super();
		this.id = id;
		this.genres = genres;
		this.rating = rating;
		this.gid = gid;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	public int getRating() {
		return rating;
	}
	public void setRating(int rating) {
		this.rating = rating;
	}
	public int getGid() {
		return gid;
	}
	public void setGid(int gid) {
		this.gid = gid;
	}
	@Override
	public String toString() {
		return "GenresAndRating [id=" + id + ", genres=" + genres + ", rating=" + rating + ", gid=" + gid + "]";
	}
	
}





